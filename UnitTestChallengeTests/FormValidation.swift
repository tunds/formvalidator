//
//  FormValidation.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class FormValidation: XCTestCase {

    private var validFormItems: [FormItem]!
    private var invalidEmailFormItems: [FormItem]!
    private var invalidTextRangeFormItems: [FormItem]!
    private var invalidPhoneNumberFormItems: [FormItem]!
    private var invalidRadioOptionFormItems: [FormItem]!
    private var invalidDateFormItems: [FormItem]!
    private var formValidator: FormValidator!
    
    override func setUp() {
        super.setUp()
        
        validFormItems = [
            EmailFormItem(email: "Tunde.Adegoroye@degree53.com"),
            TextFormItem(text: "Lorem Ipsum is"),
            PhoneFormItem(phoneNumber: "07985764773"),
            RadioFormItem(state: true),
            DateFormItem(dateString: "23/11/2018")
        ]
        
        invalidEmailFormItems = [
            EmailFormItem(email: "tunds.con"),
            TextFormItem(text: "Lorem Ipsum is"),
            PhoneFormItem(phoneNumber: "07985764773"),
            RadioFormItem(state: false),
            DateFormItem(dateString: "55/11/2018")
        ]
        
        invalidTextRangeFormItems = [
            EmailFormItem(email: "tundsdev@yahoo.co.uk"),
            TextFormItem(text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
            PhoneFormItem(phoneNumber: "07985764773"),
            RadioFormItem(state: false),
            DateFormItem(dateString: "55/11/2018")
        ]
        
        invalidPhoneNumberFormItems = [
            EmailFormItem(email: "tundsdev@yahoo.co.uk"),
            TextFormItem(text: "Lorem Ipsum is."),
            PhoneFormItem(phoneNumber: "0-3434207985764773"),
            RadioFormItem(state: false),
            DateFormItem(dateString: "55/11/2018")
        ]
        
        invalidRadioOptionFormItems = [
            EmailFormItem(email: "tundsdev@yahoo.co.uk"),
            TextFormItem(text: "Lorem Ipsum is."),
            PhoneFormItem(phoneNumber: "07985764773"),
            RadioFormItem(state: nil),
            DateFormItem(dateString: "55/11/2018")
        ]
        
        invalidDateFormItems = [
            EmailFormItem(email: "tundsdev@yahoo.co.uk"),
            TextFormItem(text: "Lorem Ipsum is."),
            PhoneFormItem(phoneNumber: "07985764773"),
            RadioFormItem(state: true),
            DateFormItem(dateString: "55/11/2018")
        ]
        
        formValidator = FormValidator()
    }

    override func tearDown() {
       super.tearDown()
    }

    func testFormTextMinLimitSet() {
        
        let newMinLimit = 0
        formValidator.setMinCharacterLimit(newMinLimit)
        XCTAssertEqual(formValidator.getMinFormTextLimit(), newMinLimit, "The form's text min limit should be: \(newMinLimit)")
    }
    
    func testFormTextMaxLimitSet() {
        
        let newMaxLimit = 30
        formValidator.setMaxCharacterLimit(newMaxLimit)
        XCTAssertEqual(formValidator.getMaxFormTextLimit(), newMaxLimit, "The form's text max limit should be: \(newMaxLimit)")
    }
    
    func testFormTextRangeSet() {
        
        let newMinLimit = 0
        let newMaxLimit = 50

        formValidator.setFormCharacterRange(newMinLimit, newMaxLimit)
        
        let formTextRange: (minLimit: Int, maxLimit: Int)  = formValidator.getFormTextCharacterRange()
        let expectedFormTextRange: (minLimit: Int, maxLimit: Int) = (minLimit: newMinLimit, maxLimit: newMaxLimit)
        
        XCTAssertTrue(formTextRange == expectedFormTextRange)
    }
    
    func testFormIsValid() {
        
        XCTAssertEqual(formValidator.validate(validFormItems), ValidationState.success, "The form should return a success")
    }
    
    func testFormIsInvalidEmail() {
        
        XCTAssertEqual(formValidator.validate(invalidEmailFormItems), ValidationState.fail(error: .invalidEmail), "The form should return a fail that there is invalid email")
    }
    
    func testFormIsInvalidTextRange() {
        
        XCTAssertEqual(formValidator.validate(invalidTextRangeFormItems), ValidationState.fail(error: .invalidCharacterRangeLimit), "The form should return a fail that there is invalid textrange")
    }
    
    func testFormIsInvalidPhoneNumber() {
        
        XCTAssertEqual(formValidator.validate(invalidPhoneNumberFormItems), ValidationState.fail(error: .invalidUKPhoneNumber), "The form should return a fail that there is invalid phone number")
    }
    
    func testFormIsInvalidRadioOptionSet() {
        
        XCTAssertEqual(formValidator.validate(invalidRadioOptionFormItems), ValidationState.fail(error: .radioOptionNotSelected), "The form should return a fail that there is unselected radio option")
    }
    
    func testFormIsInvalidDate() {
        
        XCTAssertEqual(formValidator.validate(invalidDateFormItems), ValidationState.fail(error: .invalidDate), "The form should return a fail that there is an invalid date string")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
