//
//  EmailValidationTests.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class EmailValidationTests: XCTestCase {

    private var emptyEmail = ""
    private var invalidEmail = "TundeAdegoroyedegree53.com"
    private var validEmail = "Tunde.Adegoroye@degree53.com"
    
    private var regexPatterns: [Regex]!
    private var regexValidator: RegexValidator!
    
    override func setUp() {
        super.setUp()
        
        // Initialise all the patterns
        regexPatterns = [Regex(pattern: RegexPatterns.validEmail,  error: .invalidEmail)]
        regexValidator = RegexValidator(regexCollection: regexPatterns)
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
    }

    func testValidEmail() {
        
        XCTAssertEqual(regexValidator.isValid(validEmail), ValidationState.success, "The email should be valid and return a success")
    }
    
    func testInvalidEmail() {
        
        XCTAssertEqual(regexValidator.isValid(invalidEmail), ValidationState.fail(error: .invalidEmail), "The email should be invalid and return a fail with a type of invalid email")
    }
    
    func testEmptyEmail() {
        
        XCTAssertEqual(regexValidator.isValid(emptyEmail), ValidationState.fail(error: .emptyString), "The email should be invalid and return a fail with a type of empty string")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
