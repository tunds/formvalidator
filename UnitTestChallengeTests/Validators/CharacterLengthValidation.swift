//
//  CharacterLengthValidation.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class CharacterLengthValidation: XCTestCase {

    private var minCharacterLength = 5
    private var maxCharacterLength = 10
    
    private var characterValidator: CharacterValidator!

    override func setUp() {
        super.setUp()
       characterValidator = CharacterValidator(minLimit: minCharacterLength, maxLimit: maxCharacterLength)
    }

    override func tearDown() {
        super.tearDown()
        
    }
    
    func testSettingMinLength() {
        
        let newMinLimit = 0
        characterValidator.setMinLimit(newMinLimit)
        XCTAssertEqual(characterValidator.getMinLimit(), newMinLimit, "The min limit should be equal to \(newMinLimit)")
    }
    
    func testSettingMaxLength() {
        
        let newMaxLimit = 30
        characterValidator.setMaxLimit(newMaxLimit)
        XCTAssertEqual(characterValidator.getMaxLimit(), newMaxLimit, "The min limit should be equal to \(newMaxLimit)")
    }
    
    func testCharacterLengthMeetsMinLength() {
       
        let text = "Hello there"
        XCTAssertEqual(characterValidator.isWithinMinRange(text), ValidationState.success, "The character within the text should meet the min amount of characters needed")
        
    }
    
    func testCharacterLengthMeetsMaxLength() {
        
        let text = "Hello guy"
        XCTAssertEqual(characterValidator.isWithinMaxRange(text), ValidationState.success, "The character within the text should meet the max amount of characters needed")
        
    }
    
    func testCharacterLengthIsWithinMinAndMaxLength() {
        
        let text = "Hello guy"
        XCTAssertEqual(characterValidator.isWithinCharacterRange(text), ValidationState.success, "The character within the text should be between the min and max amount of characters needed")
    }
    
    func testCharacterLengthDoesntMeetMinLength() {
        
        let text = "Hel"
        XCTAssertEqual(characterValidator.isWithinMinRange(text), ValidationState.fail(error: .invalidCharacterMinLimit), "The character within the text shouldn't meet the min amount of characters needed")
    }
    
    func testCharacterLengthDoesntMeetMaxLength() {
        
        let text = "Hello there person nice to meet you"
        XCTAssertEqual(characterValidator.isWithinMaxRange(text), ValidationState.fail(error: .invalidCharacterMaxLimit), "The character within the text shouldn't meet the max amount of characters needed")
    }
    
    func testCharacterLengthIsNotWithinMinAndMaxLength() {
        
        let text = "Hello there person nice to meet you here"
        XCTAssertEqual(characterValidator.isWithinCharacterRange(text), ValidationState.fail(error: .invalidCharacterRangeLimit), "The character within the text shouldn't be between the min and max amount of characters needed")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
