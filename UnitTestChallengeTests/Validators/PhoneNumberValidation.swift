//
//  PhoneNumberValidation.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class PhoneNumberValidation: XCTestCase {

    private var validUKPhoneNumber = "07983456774"
    private var validPlusUKPhoneNumber = "+447911123456"

    private var invalidUKPhoneNumber = "001-541-754-3010"
    private var invalidPlusUKPhoneNumber = "+1-541-754-3010"

    private var regexPatterns: [Regex]!
    private var regexValidator: RegexValidator!
    
    override func setUp() {
        super.setUp()
        
        regexPatterns = [Regex(pattern: RegexPatterns.validUKPhoneNumber,  error: .invalidUKPhoneNumber)]
        regexValidator = RegexValidator(regexCollection: regexPatterns)
    }

    override func tearDown() {
        super.tearDown()
    
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testValidUKPhoneNumber() {
        XCTAssertEqual(regexValidator.isValid(validUKPhoneNumber), ValidationState.success, "The phone should be UK phone number and return a success")
    }
    
    func testValidPlusUKPhoneNumber() {
        XCTAssertEqual(regexValidator.isValid(validPlusUKPhoneNumber), ValidationState.success, "The phone should be UK phone number and return a success")
    }
    
    func testInvalidUKPhoneNumber() {
        XCTAssertEqual(regexValidator.isValid(invalidUKPhoneNumber), ValidationState.fail(error: .invalidUKPhoneNumber), "The phone shouldn't be UK phone number and return a success")
    }
    
    func testInvalidPlusUKPhoneNumber() {
        XCTAssertEqual(regexValidator.isValid(invalidPlusUKPhoneNumber), ValidationState.fail(error: .invalidUKPhoneNumber), "The phone shouldn't be UK phone number and return a success")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
