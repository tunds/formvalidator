//
//  DateValidation.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class DateValidation: XCTestCase {

    private var dateValidator: DateValidator!
    private var defaultDateFormat = "dd/mm/yyyy"
    
    override func setUp() {
        super.setUp()
        dateValidator = DateValidator(dateFormat: defaultDateFormat)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDateFormatChanged() {
        
        let shortDateFormat = "MMM d, yyyy"
        dateValidator.setDateFormat(shortDateFormat)
        XCTAssertEqual(dateValidator.getDateFormat(), shortDateFormat, "The date shouldn't be in the following format: \(shortDateFormat)")
        dateValidator.setDateFormat(shortDateFormat)
    }
    
    func testDoubleDigitDateIsValid() {
        
        dateValidator.setDateFormat(defaultDateFormat)
        XCTAssertEqual(dateValidator.isValidDate("23/10/1994"), ValidationState.success, "The result should be a success with the following format: \(defaultDateFormat)")
    }
    
    func testSingleDigitDateIsValid() {
        
        dateValidator.setDateFormat(defaultDateFormat)
        XCTAssertEqual(dateValidator.isValidDate("2/1/1994"), ValidationState.success, "The result should be a success with the following format: \(defaultDateFormat)")
    }
    
    func testDateIsInvalid() {
        
        dateValidator.setDateFormat(defaultDateFormat)
        XCTAssertEqual(dateValidator.isValidDate("50/2/1994"), ValidationState.fail(error: .invalidDate), "The result should be a fail(invalidDate) this the correct format: \(defaultDateFormat)")
    }

    func testInputDateIsInvalid() {
        
        dateValidator.setDateFormat(defaultDateFormat)
        XCTAssertEqual(dateValidator.isValidDate("Man like"), ValidationState.fail(error: .invalidDate), "The result should be a fail(invalidDate) this the correct format: \(defaultDateFormat)")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
