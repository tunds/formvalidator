//
//  RadioValidation.swift
//  UnitTestChallengeTests
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import XCTest

@testable import UnitTestChallenge

class RadioValidation: XCTestCase {

    private var radioValidator: RadioValidator!
    
    override func setUp() {
        super.setUp()
        
        radioValidator = RadioValidator()
    }

    override func tearDown() {
       super.tearDown()
      
    }

    func testYesSelected() {
       
        radioValidator.set(true)
        XCTAssertEqual(radioValidator.getSelectedState(), true, "The state should be yes aka true")
    }

    func testNoSelected() {
        
        radioValidator.set(false)
        XCTAssertEqual(radioValidator.getSelectedState(), false, "The state should be no aka false")
    }
    
    func testNoOptionSet() {
        
        radioValidator.set(nil)
        XCTAssertEqual(radioValidator.getSelectedState(), nil, "The state should be nil")
    }
    
    func testOptionNotSelected() {
        
        radioValidator.set(nil)
        XCTAssertEqual(radioValidator.isValueSelected(), ValidationState.fail(error: .radioOptionNotSelected), "The state should be fail(radioOptionNotSelected) since no option selected")
    }
    
    func testOptionHasBeenSelected() {
        
        radioValidator.set(true)
        XCTAssertEqual(radioValidator.isValueSelected(), ValidationState.success, "The state should be success since an option has been selected")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
