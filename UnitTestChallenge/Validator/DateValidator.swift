//
//  DateValidator.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import Foundation

final class DateValidator {
    
    private var dateFormat: String!
    
    init(dateFormat: String) {
        self.dateFormat = dateFormat
    }
    
    func setDateFormat(_ format: String) {
        self.dateFormat = format
    }
    
    func getDateFormat() -> String{
       return self.dateFormat
    }
    
    func isValidDate(_ dateString: String) -> ValidationState {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: dateString) != nil ? ValidationState.success : ValidationState.fail(error: .invalidDate)
    }
    
}
