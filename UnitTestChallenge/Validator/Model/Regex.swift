//
//  Regex.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

public struct Regex {
    var pattern: String
    var error: ValidationError
    
    public init(pattern: String, error: ValidationError) {
        self.pattern = pattern
        self.error = error
    }
}
