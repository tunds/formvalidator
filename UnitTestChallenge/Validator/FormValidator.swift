//
//  FormValidator.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import Foundation

final class FormValidator {
    
    private lazy var emailRegexValidator = RegexValidator(regexCollection: [Regex(pattern: RegexPatterns.validEmail,  error: .invalidEmail)])
    private lazy var characterTextValidator = CharacterValidator(minLimit: 10, maxLimit: 20)
    private lazy var ukPhoneNumberRegexValidator = RegexValidator(regexCollection: [Regex(pattern: RegexPatterns.validUKPhoneNumber,  error: .invalidUKPhoneNumber)])
    private lazy var radioValidator = RadioValidator()
    private lazy var dateValidator = DateValidator(dateFormat: "dd/mm/yyyy")

    func validate(_ items: [FormItem]) -> ValidationState {
        
        for item in items {
         
            switch item {
            case is EmailFormItem:
                let emailFormItem = item as! EmailFormItem
                let result = emailRegexValidator.isValid(emailFormItem.email)
                if result != .success {
                    return result
                }
            case is TextFormItem:
                let textFormItem = item as! TextFormItem
                let result = characterTextValidator.isWithinCharacterRange(textFormItem.text)
                if result != .success {
                    return result
                }
            case is PhoneFormItem:
                let phoneFormItem = item as! PhoneFormItem
                let result = ukPhoneNumberRegexValidator.isValid(phoneFormItem.phoneNumber)
                if result != .success {
                    return result
                }
            case is RadioFormItem:
                let radioFormItem = item as! RadioFormItem
                radioValidator.set(radioFormItem.state)
                let result = radioValidator.isValueSelected()
                if result != .success {
                    return result
                }
            case is DateFormItem:
                let dateFormItem = item as! DateFormItem
                let result = dateValidator.isValidDate(dateFormItem.dateString)
                if result != .success {
                    return result
                }
            default:
                break
                
            }
        }
        
        return .success
    }
    
    func setMinCharacterLimit(_ minLimit: Int) {
        characterTextValidator.setMinLimit(minLimit)
    }
    
    func setMaxCharacterLimit(_ maxLimit: Int) {
        characterTextValidator.setMaxLimit(maxLimit)
    }
    
    func setFormCharacterRange(_ minLimit: Int, _ maxLimit: Int) {
        characterTextValidator.setMinLimit(minLimit)
        characterTextValidator.setMaxLimit(maxLimit)
    }
    
    func getMinFormTextLimit() -> Int {
        return self.characterTextValidator.getMinLimit()
    }
    
    func getMaxFormTextLimit() -> Int {
        return self.characterTextValidator.getMaxLimit()
    }
    
    func getFormTextCharacterRange() -> (minLimit: Int, maxLimit: Int) {
        return (minLimit: self.characterTextValidator.getMinLimit(), maxLimit: self.characterTextValidator.getMaxLimit())
    }
}
