//
//  CharacterValidator.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import Foundation

final class CharacterValidator {
    
    private var minLimit: Int!
    private var maxLimit: Int!
    
    init(minLimit: Int, maxLimit: Int) {
        self.minLimit = minLimit
        self.maxLimit = maxLimit
    }
    
    func isWithinCharacterRange(_ text: String) -> ValidationState {
        return text.count >= minLimit && text.count <= maxLimit ? .success : .fail(error: .invalidCharacterRangeLimit)
    }
    
    func isWithinMinRange(_ text: String) -> ValidationState {
        return text.count >= minLimit ? .success : .fail(error: .invalidCharacterMinLimit)
    }
    
    func isWithinMaxRange(_ text: String) -> ValidationState {
        return text.count <= maxLimit ? .success : .fail(error: .invalidCharacterMaxLimit)
    }
    
    func setMinLimit(_ minLimit: Int) {
        self.minLimit = minLimit
    }
    
    func setMaxLimit(_ maxLimit: Int) {
        self.maxLimit = maxLimit
    }
    
    func getMinLimit() -> Int {
       return self.minLimit
    }
    
    func getMaxLimit() -> Int {
        return self.maxLimit
    }
}
