//
//  RadioValidator.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import Foundation

final class RadioValidator {
    
    private var state: Bool?
    
    func set(_ state: Bool?) {
        self.state = state
    }
    
    func isValueSelected() -> ValidationState {
        return state != nil ? ValidationState.success : ValidationState.fail(error: .radioOptionNotSelected)
    }
    
    func getSelectedState() -> Bool? {
        return self.state
    }
    
}
