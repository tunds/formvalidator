//
//  RegexValidator.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

import Foundation

public final class RegexValidator {
    
    fileprivate var patterns =  [NSRegularExpression]()
    fileprivate var regexCollection = [Regex]()
    
    public init(regexCollection: [Regex]) {
        self.regexCollection = regexCollection
        do {
            self.patterns = try regexCollection.map { try NSRegularExpression(pattern: $0.pattern) }
        } catch {
            preconditionFailure("Illegal Regular expression: \(error.localizedDescription)")
        }
    }
    
    /// Checks to see if the string values matches the regex expression
    ///
    /// - Parameters:
    ///   - text: The string value
    ///   - result: Did the value match or not? Was it a success or a fail?
    public func isValid(_ text: String) -> ValidationState {
                
        if patterns.count > 0 {
            
            for regexPattern in patterns {
                
                guard !text.isEmpty || text.contains("\n") else {
                    return .fail(error: .emptyString)
                }
                
                if !matches(pattern: regexPattern, text) {
                    if let regex = regexCollection.filter ({ $0.pattern == regexPattern.pattern }).first {
                        return .fail(error: regex.error)
                    }
                }
            }
            
        }
        
        return ValidationState.success
    }
}

private extension RegexValidator {
    /// Checks to see if a value matches the pattern
    ///
    /// - Parameters:
    ///   - pattern: The regular expression
    ///   - string: The value to match it against
    /// - Returns: A Boolean that returns if the pattern match or not
    func matches(pattern: NSRegularExpression, _ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return pattern.firstMatch(in: string, options: [], range: range) != nil
    }
}
