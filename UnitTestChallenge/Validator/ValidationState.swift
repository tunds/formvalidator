//
//  ValidationState.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

public enum ValidationState: Equatable {
    case success
    case fail(error: ValidationError)
}

public enum ValidationError: Error {
    case na
    case invalidEmail
    case emptyString
    case invalidAge
    case invalidPassword
    case invalidCharacterRangeLimit
    case invalidCharacterMinLimit
    case invalidCharacterMaxLimit
    case invalidUKPhoneNumber
    case radioOptionNotSelected
    case invalidDate
}

