//
//  EmailFormItem.swift
//  UnitTestChallenge
//
//  Created by Tunde on 19/11/2018.
//  Copyright © 2018 Degree53. All rights reserved.
//

struct EmailFormItem: FormItem {
    let email: String
}
